import dbHandler
from terminal import terminal as Terminal

database:dbHandler = dbHandler.db("database.sqlite")
terminal = Terminal(database)

try:
    terminal.main()
finally:
    database.close()