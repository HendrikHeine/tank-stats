import os
import datetime
import time
import dbHandler

class terminal:
    def __init__(self, db) -> None:
        self.__db:dbHandler.db = db
        self.__time = time

    def __getDateTime(self):
        dateNow = datetime.datetime.now()
        dateNow = str(dateNow).split(" ")
        unix = self.__time.time()
        time = dateNow[1]
        time = time.split(".")
        time = time[0]
        dateNow = dateNow[0]
        value = [ dateNow, time, unix ]
        return value


    def main(self):
        os.system("clear")
        print("/---------------------------------------------------------\\")
        print(f"|                     Tank Stats                          |")
        print(f"| (0) Exit                                                |")
        print(f"| (1) Neuer Eintrag                                       |")
        print(f"| (2) Einträge anschaunen/bearbeiten                      |")
        print("\\---------------------------------------------------------/")

        try:
            userInput = int(input("\n>>> "))
        except:
            self.main()
        
        if userInput != None:
            if userInput in range(0, 6):
                if userInput == 0:
                    exit(0)
                if userInput == 1:
                    self.newEntry()
                if userInput == 2:
                    self.showEditDelete()
            else:
                self.main()
        else:
            exit(0)

    def newEntry(self):
        os.system("clear")
        print("/---------------------------------------------------------\\")
        print(f"|                   Neuer Eintrag                         |")
        print(f"| (0) Exit                                                |")

        try:
            userInputLiter = float(input("|\n| Liter >>> "))
            userInputPrice = float(input("| Preis >>> "))
        except:
            self.newEntry()
        
        if userInputLiter != None and userInputLiter != 0.0 and userInputPrice != None and userInputPrice != 0.0:
                value = self.__db.writeToDB(date=self.__getDateTime()[2], price=userInputPrice, liter=userInputLiter)
                if value[0] == 0:
                    print(f"|                    Gespeichert                          |")
                    print("\\---------------------------------------------------------/")
                    input("...")
                else:
                    print(f"|                       Fehler                            |")
                    print(f"| {value[1]}")
                    print("\\---------------------------------------------------------/")
                    input("...")
                self.main()
        else:
            self.main()

    def showEditDelete(self):
        os.system("clear")
        print("/---------------------------------------------------------\\")
        print(f"|                        Stats                            |")
        value = self.__db.getStats()
        if value[0] == 1:
            print(f"|                       Fehler                            |")
            print(f"| {value[1]}")
            print("\\---------------------------------------------------------/")
            input("...")
        else:
            totalLiter = 0.0
            totalPrice = 0.0
            print(f"| Date              | Price     | Liter                   |")
            for stat in value[1]:
                totalLiter += stat[2]
                totalPrice += stat[3]
                date = (datetime.datetime.utcfromtimestamp(stat[1]).strftime('%d.%m.%Y %H:%M:%S')).split(" ")
                if stat[2] <= 9:
                    priceString = f"{stat[2]}         |"
                else:
                    priceString = f"{stat[2]}\t|"
                
                print(f"| {date[0]}        | {priceString} {stat[2]}")
            
            print("|---------------------------------------------------------|")
            print(f"| Total Liter: {totalLiter}L")
            print(f"| Total Price: {totalPrice}€")
            print("\\---------------------------------------------------------/")
            input("...")
        self.main()


