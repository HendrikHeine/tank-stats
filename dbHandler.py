import sqlite3

class db:
    def __init__(self, database) -> None:
        self.__connection = sqlite3.connect(database)
        self.__cursor = self.__connection.cursor()
        self.__buildDB()

    def __buildDB(self):
        try:
            with open('schema.sql') as schema:
                try:
                    self.__cursor.executescript(schema.read())
                    return 0
                except:return 1
        except:return 2


    def close(self):
        self.__cursor.close()
        self.__connection.close()

    def writeToDB(self, date:float, price:float, liter:float):
        query = "INSERT INTO stats (date, price, liter) VALUES (?, ?, ?)"
        data = (date, price, liter)
        try:
            self.__cursor.execute(query, data)
            self.__connection.commit()
            return [0]
        except sqlite3.Error as err:
            return [1, err]

    def getStats(self):
        query = "SELECT * FROM stats"
        try:
            self.__cursor.execute(query)
            return [0, self.__cursor.fetchall()]
        except sqlite3.Error as err:
            return [1, err]

    